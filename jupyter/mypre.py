import os
import sys
import natsort
import preprocesor as pr
import cocomaker
import config
import coco_train
import coco_test
import res_show
import res_eval
import resnet
import cv2

import numpy as np
from skimage.io import imread, imshow, imsave

sys.path.append(config.LIB_MRCNN_PATH)
sys.path.append(config.LIB_COCO_PATH)

import mrcnn.model as model_mask_lib
from mrcnn import utils

sys.path.append('D:\Workspace_Dental\dental_workspace\python')
from implant_recog_conf import Implant_Recog_Config
from img_processing import reshape_img, cutout
from glob import glob
from os import path as pathpackage

class Pred(object):
    def __init__(self, rcnn_model_path, resnet_model_path=None, resnet_class_map_path=None, working_dir='.', config_ = None):
    #def __init__(self, model_path, config_=None):
        if config_ == None:
            self.config = config.InferenceConfig()
        else:
            self.config = config_

        self.model = model_mask_lib.MaskRCNN(mode="inference",
                                        config=self.config,
                                             model_dir='.')

        #model_path = self.model.find_last()
        print("Loading weights from ", rcnn_model_path)
        self.model.load_weights(rcnn_model_path, by_name=True)

        if resnet_model_path is not None and resnet_class_map_path is not None:
            self.resnet_config = self.config.ResnetConfig()
            self.pre = pr.preprocessor(working_dir, '', None, None, self.resnet_config)
            self.pre.load_cat_class_map(resnet_class_map_path)

            classes_number = len(self.pre.cat_class_map)
            self.resnet_model = resnet.build_resnet50(self.resnet_config.img_shape, self.resnet_config.mask_shape,
                                                      classes_number, show_model=False)

            self.resnet_model.load_weights(resnet_model_path)
        else:
            self.pre = pr.preprocessor(working_dir)

    def detect(self, path, im=None):
        if im is None:
            im = imread(path)
        results = self.model.detect([im], verbose=1)
        masks = results[0]['masks']
        res = []

        for i in range(masks.shape[2]):
            res.append(masks[:, :, i])

        return res


    def detect_and_label(self, path, im=None):
        if im is None:
            im = imread(path)
        results = self.model.detect([im], verbose=1)
        masks = results[0]['masks']
        res = []

        for i in range(masks.shape[2]):
            res.append(masks[:, :, i])

        new_masks, boxs, labels = resnet.predict(self.pre, self.resnet_model, im, res)

        return new_masks, labels


    def detect_and_label_show(self, path, save_path):

        # im_raw = imread(path)

        # im, window, scale, padding, crop = utils.resize_image(
        #     im_raw,
        #     min_dim=self.config.IMAGE_MIN_DIM,
        #     min_scale=self.config.IMAGE_MIN_SCALE,
        #     max_dim=self.config.IMAGE_MAX_DIM,
        #     mode=self.config.IMAGE_RESIZE_MODE)
        im = imread(path)
        results = self.model.detect([im], verbose=1)
        masks = results[0]['masks']
        res = []

        for i in range(masks.shape[2]):
            res.append(masks[:, :, i])

        new_masks, boxs, labels = resnet.predict(self.pre, self.resnet_model, im, res)

        rois = np.stack(boxs)


        scale = 256.0 / im.shape[0]
        new_width = int(im.shape[1] * scale)
        im_small = cv2.resize(im, (new_width, 256))
        rois_small = (rois*scale).astype(np.int)
        masks_small = [cv2.resize(mask.astype(np.uint8), (new_width, 256)).astype(np.bool) for mask in new_masks]
        new_masks_stack = np.stack(masks_small, axis=-1)


        print("filename {} rois {}", save_path, rois.shape[0])
        mask = np.zeros([im.shape[0], im.shape[1], rois.shape[0]])

        np.savez(save_path + 'one.npz',
                 rois=rois_small, masks=new_masks_stack, class_ids=labels, scores=results[0]['scores'],
                 mask=mask, img=im_small, img_id=0, filename=os.path.basename(path))


        file_name = os.path.splitext(os.path.basename(path))[0] + '.result.png'
        res_show.show_one(save_path + 'one.npz', save_path)
        # res_show.export_one(r, file_name)


    def export_all(self, model_path, file_paths):
        pred = Pred(model_path)

        imgs = glob(os.path.join(file_paths, '*'))
        results=[]
        for i, path in enumerate(imgs):
            print(path)
            name_ext = os.path.basename(path)
            name, ext = os.path.splitext(name_ext)
            print("filepath ", path)
            print("exportname ", name + "_res.png")
            pred.detect_and_show(path, name + "_res.png")

    def detect_all(self, d):
        recog_conf = Implant_Recog_Config()
        group = os.path.basename(d)
        imgs = glob(os.path.join(d, '*'))
        results=[]
        for i, path in enumerate(imgs):
            print(path)
            # new
            im_raw = imread(path)
            npz_name = os.path.splitext(os.path.basename(path))[0] + '.npz'
            npz_dir = os.path.join(recog_conf.detectpath, group)

            file_name = os.path.splitext(os.path.basename(path))[0] + '.result.png'

            if pathpackage.exists(file_name):
                print("skipping: ", file_name)
                continue

            results = []

            im, window, scale, padding, crop = utils.resize_image(
                im_raw,
                min_dim=self.config.IMAGE_MIN_DIM,
                min_scale=self.config.IMAGE_MIN_SCALE,
                max_dim=self.config.IMAGE_MAX_DIM,
                mode=self.config.IMAGE_RESIZE_MODE)
            results = self.model.detect([im], verbose=1)
            exportimg = [1]
            exportimg = results[0]

            res = []
            mask=[]

            mask = np.zeros([im.shape[0], im.shape[1], exportimg['rois'].shape[0]])

            exportimg['mask'] = mask
            exportimg['img'] = im
            exportimg['img_id'] = 0
            exportimg['filename'] = os.path.basename(path)

            # np.savez('one.npz',
            #          rois=r['rois'], masks=r['masks'], class_ids=r['class_ids'], scores=r['scores'],
            #          mask=mask, img=im, img_id=0, filename=os.path.basename(path))

            # file_name = os.path.splitext(os.path.basename(path))[0] + '.result.png'
            #
            res_show.export_one(exportimg, file_name)

    def detect_and_show(self, path, save_path, preview=True):

        im_raw = imread(path)
        if len(im_raw.shape) < 3:
            im_raw = np.dstack([im_raw, im_raw, im_raw])

        im, window, scale, padding, crop = utils.resize_image(
            im_raw,
            min_dim=self.config.IMAGE_MIN_DIM,
            min_scale=self.config.IMAGE_MIN_SCALE,
            max_dim=self.config.IMAGE_MAX_DIM,
            mode=self.config.IMAGE_RESIZE_MODE)


        results = self.model.detect([im], verbose=1)
        r = results[0]
        res = []

        print("filename {} rois {}", save_path, r['rois'].shape[0])
        mask = np.zeros([im.shape[0], im.shape[1], r['rois'].shape[0]])

        # res['rois'] = r['rois']
        # res['masks'] = r['masks']
        # res['class_ids'] = r['class_ids']
        # res['scores'] = r['scores']
        # res['rois'] = r['rois']
        # res['mask'] = mask
        # res['img'] = im
        # res['img_id'] = 0
        # res['filename'] = os.path.basename(path)

        # print("test: ", r)
        npzname = os.path.splitext(save_path)[0] + '.npz'
        np.savez(npzname,
                 rois=r['rois'], masks=r['masks'], class_ids=r['class_ids'], scores=r['scores'],
                 mask=mask, img=im, img_id=0, filename=os.path.basename(path))


        #file_name = os.path.splitext(os.path.basename(path))[0] + '.result.png'
        if preview:
            res_show.show_one(npzname, save_path)
        # res_show.export_one(r, file_name)


def main(argv):
    model_path = r"D:\Workspace_Dental\mask_rcnn_implant.h5"

    inputfile = ''
    mode = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","mmode="])
    except getopt.GetoptError:
        print( 'test.py -i <inputfile> -m <mode detect or detect_show>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print ('test.py -i <inputfile> -m <detect or detect_show>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-m", "--mmode"):
            mode = arg
    print( 'Input file is "'+ inputfile)
    print('Mode file is "', mode)
    #file_path = r"D:\Workspace_Dental\database\image_mini\20160618164614.png"
    #file_path = r"20160613091054.png"
    file_path = r"20160604095540.png"

    
    pred = Pred(model_path)

    if mode=='detect_show':
        name_ext = os.path.basename(inputfile)
        name, ext = os.path.splitext(name_ext)
        print("filepath ", inputfile)
        print("exportname ", name + "_res.png")
        pred.detect_and_show(inputfile, name + "_res.png")

    if mode=='detect':
        pred.detect(inputfile)

if __name__ == '__main__':



    dbpath = r"D:\Workspace_Dental\database\integrated_model"
    teeth_dct_model_path = os.path.join(dbpath, 'model', 'teeth20200518T0446', 'mask_rcnn_teeth_0225.h5')
    teeth_dct_inf_config = config.InferenceConfig()
    teeth_dct_model = Pred(teeth_dct_model_path, config_=teeth_dct_inf_config)

    print(dbpath)

    pre = pr.preprocessor(dbpath, None, None, None)
    targets = []
    trgtdir = os.path.join(dbpath,  "cocoset", "train2019")
    pre.walk_files(trgtdir, targets, ".png")

    for file_path in targets:
        name_ext = os.path.basename(file_path[0])
        name, ext = os.path.splitext(name_ext)

        print("predict: ", name_ext)
        outdir = os.path.join(dbpath, "tmp_res")
        if not os.path.isdir(outdir):
            os.makedirs(outdir, exist_ok=True)
        outfile = os.path.join(outdir, name+"_dct.png")
        teeth_dct_model.detect_and_show(file_path[0], outfile, preview=True)

        break

















