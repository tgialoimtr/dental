import sys
sys.path.append("/home/loitg/workspace/dental/Dental_MRCNN")

from mrcnn.config import Config

class TeethConfig(Config):

    NAME = "teeth"

    GPU_COUNT = 1
    #IMAGES_PER_GPU = 4
    IMAGES_PER_GPU = 1
    #IMAGES_PER_GPU = 8

    NUM_CLASSES = 1 + 1  # background + 3 shapes for teeth only
    #NUM_CLASSES = 3 + 1  # background + 3 shapes for bridge
    #NUM_CLASSES = 5 + 1  # background + 3 shapes for integrated model
    #NUM_CLASSES = 4 + 1  # background + 3 shapes for integrated model without teeth

    IMAGE_MIN_DIM = 256
    IMAGE_MAX_DIM = 512

    RPN_ANCHOR_SCALES = (16, 32, 64, 128, 256)

    TRAIN_ROIS_PER_IMAGE = 80
    #TRAIN_ROIS_PER_IMAGE = 20

    STEPS_PER_EPOCH = 80
    #STEPS_PER_EPOCH = 10

    VALIDATION_STEPS = 50
    #VALIDATION_STEPS = 5


    #MY_TRAIN_HEAD_EPOCH = 1
    MY_TRAIN_HEAD_EPOCH = 75

    #MY_TRAIN_BODY_EPOCH = 1
    MY_TRAIN_BODY_EPOCH = 75

    #MY_TRAIN_FULL_EPOCH = 1
    MY_TRAIN_FULL_EPOCH = 75



class InferenceConfig(TeethConfig):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

class BridgeInferenceConfig(InferenceConfig):
    #NUM_CLASSES = 1+3
    NUM_CLASSES = 1+4



class ResnetConfig():
    img_shape = (224, 224, 3)
    mask_shape = (224, 224, 3)


class ResnetConfig():
    img_shape = (224, 224, 3)
    mask_shape = (224, 224, 3)